import 'dart:io' show exitCode, stdin, stdout;

class Menu {
  var choice,
      level,
      prod,
      menu,
      price,
      pay,
      paymoney,
      list,
      lev,
      pro,
      confirm,
      type,
      val,
      hot,
      cool,
      mix,
      choicemenu;
  var count = 0;

  void getMenu(String choice, int price, String type, String list, String lev,
      String pro) {
    this.choice = choice;
    this.price = price;
    this.type = type;
    this.list = list;
    this.lev = lev;
    this.pro = pro;
  }

  void printMenu() {
    print('เลือกรายการที่ท่านต้องการ : ');
    menu = {
      "กด 1": "เมนูแนะนำ",
      // 'กด 2': 'กาแฟ',
      // "กด 3": "ชา",
      // 'กด 4': 'นม โกโก้และคาราเมล',
      // "กด 5": "โปรตีนเชค",
      // 'กด 6': 'น้ำโซดาและอื่นๆ',
      "exit": "EXIT"
    };
    menu.forEach((n, s) => print(' ${n}. ${s}'));

    stdout.write('กดเมนูที่ท่านต้องการเลือก: ');
    choice = (stdin.readLineSync()!);

    if (choice == "exit") {
      print('ขอบคุณที่ให้บริการนะคะ');
      exitCode;
    } else {
      Check(choice);
    }
  }

  void Check(String choice) {
    if (choice == "1") {
      Menumain(choice);
    } else if (choice == "2") {
      Menumain(choice);
    } else if (choice == "3") {
      Menumain(choice);
    } else if (choice == "4") {
      Menumain(choice);
    } else if (choice == "5") {
      Menumain(choice);
    } else if (choice == "6") {
      Menumain(choice);
    }
  }

  void Checkmenu(String choicemenu) {
    if (choicemenu == "1") {
      Type1();
      count++;
      Price(price, type, choicemenu);
    } else if (choicemenu == "2") {
      Type2();
      count++;
      Price(price, type, choicemenu);
    } else if (choicemenu == "3") {
      Type3();
      count++;
      Price(price, type, choicemenu);
    } else if (choicemenu == "4") {
      Type3();
      count++;
      Price(price, type, choicemenu);
    } else if (choicemenu == "5") {
      Type3();
      count++;
      Price(price, type, choicemenu);
    } else if (choicemenu == "6") {
      mix = 45;
      Type4();
      count++;
      Price(price, type, choicemenu);
    } else if (choicemenu == "7") {
      mix = 55;
      Type4();
      count++;
      Price(price, type, choicemenu);
    } else if (choicemenu == "8") {
      cool = 15;
      mix = 20;
      Type5();
      count++;
      Price(price, type, choicemenu);
    } else if (choicemenu == "9") {
      cool = 20;
      mix = 25;
      Type5();
      count++;
      Price(price, type, choicemenu);
    } else if (choicemenu == "10") {
      Type6();
      count++;
      Price(price, type, choicemenu);
    } else if (choicemenu == "11") {
      cool = 25;
      mix = 30;
      Type5();
      count++;
      Price(price, type, choicemenu);
    } else if (choicemenu == "12") {
      cool = 40;
      mix = 45;
      Type5();
      count++;
      Price(price, type, choicemenu);
    } else if (choicemenu == "13") {
      Type3();
      count++;
      Price(price, type, choicemenu);
    }
  }

  void Price(int price, String type, String choicemenu) {
    if (count == 1) {
      print('เลือกระดับความหวาน : ');
      menu = {
        "กด 1": "ไม่ใส่น้ำตาล",
        'กด 2': 'หวานน้อย',
        "กด 3": "หวานพอดี",
        "กด 4": "หวานมาก",
        "กด 5": "หวาน3โลก"
      };
      menu.forEach((n, s) => print(' ${n}. ${s}'));
      stdout.write('กดระดับคสามหวานที่ท่านต้องการเลือก: ');
      level = (stdin.readLineSync()!);
      print('หลอด และ ฝา : ');
      menu = {
        "กด 1": "รับหลอด",
        'กด 2': 'รับฝา',
        'กด 3': 'รับหลอด และ ฝา',
      };
      menu.forEach((n, s) => print(' ${n}. ${s}'));
      stdout.write('ต้องการรับหลอด และ ฝา ไมค่ะ: ');
      prod = (stdin.readLineSync()!);
      menu = {
        "cancel": "ยกเลิก",
        'buy': 'ซื้อเลย ($price บ.)',
      };
      menu.forEach((n, s) => print(' ${n} ${s}'));
      stdout.write('ยืนยัน: ');
      confirm = (stdin.readLineSync()!);
      if (confirm == "buy") {
        print('จ่ายช่องทางไหน: ');
        menu = {
          "กด 1": "จ่ายเงินสด",
          'กด 2': 'แสกน QR',
          "กด 3": "เต่าบินเครดิต",
        };
        menu.forEach((n, s) => print(' ${n}. ${s}'));
        stdout.write('กดตัวเลือกช่องทางจ่าย: ');
        pay = (stdin.readLineSync()!);
        if (pay == "1") {
          checkmenu(choicemenu, level, prod);
          getMenu(choice, price, type, list, lev, pro);
          stdout.writeln('รายการสั้งซื้อ $list $type ราคา $price บ. $lev $pro');
          stdout.write('จ่ายจำนวนเงิน: ');
          paymoney = int.parse(stdin.readLineSync()!);
          var num = paymoney - price;
          if (num == 0) {
            print("รับจำนวนเงินพอดี");
          } else if (paymoney >= price) {
            stdout.write('เงินทอน $num บ.');
          } else {
            print("จำนวนเงินไม่เพียงพอ");
          }
        }
      } else {
        count = 0;
        printMenu();
        exitCode;
      }
    } else {
      exitCode;
    }
  }

  void Type1() {
    print('ประเภทเครื่องดื่ม : ');
    menu = {
      "hot": "25 บ.",
      "cool": '-',
      "mix": "-",
    };
    menu.forEach((n, s) => print(' ${n} ${s}'));
    stdout.write('กดประเภทที่ท่านต้องการเลือก: ');
    type = stdin.readLineSync()!;
    if (type == "hot") {
      type = "ร้อน";
      price = 25;
    } else if (type == "cool") {
      print("ไม่มีประเภทนี้");
    } else if (type == "mix") {
      print("ไม่มีประเภทนี้");
    } else {
      printMenu();
      exitCode;
      //แก้
    }
  }

  void Type2() {
    print('ประเภทเครื่องดื่ม : ');
    menu = {
      "hot": "30 บ.",
      "cool": '25 บ.',
      "mix": "-",
    };
    menu.forEach((n, s) => print(' ${n} ${s}'));
    stdout.write('กดประเภทที่ท่านต้องการเลือก: ');
    type = stdin.readLineSync()!;
    if (type == "hot") {
      type = "ร้อน";
      price = 30;
    } else if (type == "cool") {
      type = "เย็น";
      price = 25;
    } else if (type == "mix") {
      print("ไม่มีประเภทนี้");
    } else {
      printMenu();
      exitCode;
    }
  }

  void Type3() {
    print('ประเภทเครื่องดื่ม : ');
    menu = {
      "hot": "35 บ.",
      "cool": '40 บ.',
      "mix": "45 บ.",
    };
    menu.forEach((n, s) => print(' ${n} ${s}'));
    stdout.write('กดประเภทที่ท่านต้องการเลือก: ');
    type = stdin.readLineSync()!;
    if (type == "hot") {
      type = "ร้อน";
      price = 35;
    } else if (type == "cool") {
      type = "เย็น";
      price = 40;
    } else if (type == "mix") {
      type = "ปั่น";
      price = 45;
    } else {
      printMenu();
      exitCode;
    }
  }

  void Type4() {
    print('ประเภทเครื่องดื่ม : ');
    menu = {
      "hot": "-",
      "cool": '-',
      if (mix == 45) "mix": "45 บ.",
      if (mix == 55) "mix": "55 บ.",
    };
    menu.forEach((n, s) => print(' ${n} ${s}'));
    stdout.write('กดประเภทที่ท่านต้องการเลือก: ');
    type = stdin.readLineSync()!;
    if (type == "hot") {
      print("ไม่มีประเภทนี้");
    } else if (type == "cool") {
      print("ไม่มีประเภทนี้");
    } else if (type == "mix") {
      type = "ปั่น";
      if (mix == 45) {
        price = 45;
      } else if (mix == 55) {
        price = 55;
      }
    } else {
      printMenu();
      exitCode;
    }
  }

  void Type5() {
    print('ประเภทเครื่องดื่ม : ');
    menu = {
      "hot": "-",
      if (cool == 15) "cool": "15 บ.",
      if (cool == 20) "cool": "20 บ.",
      if (cool == 25) "cool": "25 บ.",
      if (cool == 40) "cool": "40 บ.",
      if (mix == 20) "mix": "20 บ.",
      if (mix == 25) "mix": "25 บ.",
      if (mix == 30) "mix": "30 บ.",
      if (mix == 45) "mix": "45 บ.",
    };
    menu.forEach((n, s) => print(' ${n} ${s}'));
    stdout.write('กดประเภทที่ท่านต้องการเลือก: ');
    type = stdin.readLineSync()!;
    if (type == "hot") {
      print("ไม่มีประเภทนี้");
    } else if (type == "cool") {
      type = "เย็น";
      if (cool == 15) {
        price = 15;
      } else if (cool == 20) {
        price = 20;
      } else if (cool == 25) {
        price = 25;
      } else if (cool == 40) {
        price = 40;
      }
    } else if (type == "mix") {
      type = "ปั่น";
      if (mix == 20) {
        price = 20;
      } else if (mix == 25) {
        price = 25;
      } else if (mix == 30) {
        price = 30;
      } else if (mix == 45) {
        price = 45;
      }
    } else {
      printMenu();
      exitCode;
    }
  }

  void Type6() {
    print('ประเภทเครื่องดื่ม : ');
    menu = {
      "hot": "-",
      "cool": '15 บ.',
      "mix": "-",
    };
    menu.forEach((n, s) => print(' ${n} ${s}'));
    stdout.write('กดประเภทที่ท่านต้องการเลือก: ');
    type = stdin.readLineSync()!;
    if (type == "hot") {
      print("ไม่มีประเภทนี้");
    } else if (type == "cool") {
      type = "เย็น";
      price = 15;
    } else if (type == "mix") {
      print("ไม่มีประเภทนี้");
    } else {
      printMenu();
      exitCode;
    }
  }

  void Menumain(String choicemenu) {
    print('เมนูแนะนำ : ');
    menu = {
      "กด 1": "เอสเพรสโซ่: ร้อน25บ.",
      'กด 2': 'กาแฟดำ(อเมริกาโน): ร้อน30บ. , เย็น25บ.',
      "กด 3": "ลาเต้: ร้อน35บ. , เย็น40บ. , ปั่น45บ.",
      'กด 4': 'ชานมไทย: ร้อน35บ. , เย็น40บ. , ปั่น45บ.',
      "กด 5": "โกโก้: ร้อน35บ. , เย็น40บ. , ปั่น45บ.",
      'กด 6': 'นมสตอเบอร์รี่ปั่น: ปั่น45บ.',
      'กด 7': 'โอริโอ้ปั่นภูเขาไฟ: ปั่น55บ.',
      "กด 8": "เป๊ปซี่น้ำแข็ง: เย็น15บ. , ปั่น20บ.",
      'กด 9': 'เป๊ปซี่บ๊วย: เย็น20บ. , ปั่น25บ.',
      "กด 10": "เต่าทรงพลังโซดา: เย็น15บ.",
      'กด 11': 'บ๊วยมะนาวโซดา: เย็น25บ. , ปั่น30บ.',
      'กด 12': 'นมชมพู: เย็น40บ. , ปั่น45บ.',
      'กด 13': 'นมคาราเมล: ร้อน35บ. , เย็น40บ. , ปั่น45บ.',
      "back": "ย้อนกลับ"
    };
    menu.forEach((n, s) => print(' ${n}. ${s}'));
    stdout.write('กดเมนูที่ท่านต้องการเลือก: ');
    choicemenu = (stdin.readLineSync()!);
    if (choicemenu == "back") {
      return printMenu();
    }
    Checkmenu(choicemenu);
  }

  checkmenu(String choicemenu, String level, String prod) {
    switch (choicemenu) {
      case "1":
        list = "เอสเพรสโซ่";
        break;
      case "2":
        list = "กาแฟดำ(อเมริกาโน)";
        break;
      case "3":
        list = "ลาเต้";
        break;
      case "4":
        list = "ชานมไทย";
        break;
      case "5":
        list = "โกโก้";
        break;
      case "6":
        list = "นมสตอเบอร์รี่ปั่น";
        break;
      case "7":
        list = "โอริโอ้ปั่นภูเขาไฟ";
        break;
      case "8":
        list = "เป๊ปซี่น้ำแข็ง";
        break;
      case "9":
        list = "เป๊ปซี่บ๊วย";
        break;
      case "10":
        list = "เต่าทรงพลังโซดา";
        break;
      case "11":
        list = "บ๊วยมะนาวโซดา";
        break;
      case "12":
        list = "นมชมพู";
        break;
      case "13":
        list = "นมคาราเมล";
        break;
      default:
        break;
    }
    switch (level) {
      case "1":
        lev = "ไม่ใส่น้ำตาล";
        break;
      case "2":
        lev = "หวานน้อย";
        break;
      case "3":
        lev = "หวานพอดี";
        break;
      case "4":
        lev = "หวานมาก";
        break;
      case "5":
        lev = "หวาน3โลก";
        break;
      default:
        break;
    }
    switch (prod) {
      case "1":
        pro = "รับหลอด";
        break;
      case "2":
        pro = "รับฝา";
        break;
      case "3":
        pro = "รับหลอด และ ฝา";
        break;
      default:
        break;
    }
  }
}

void main() {
  var user = Menu();
  String choice;

  user.printMenu();
}
