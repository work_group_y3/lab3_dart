import 'dart:io';

class Calculat {
  var num1;
  var num2;
  var choice;

  void getNum(int num1, String choice, int num2) {
    this.num1 = num1;
    this.choice = choice;
    this.num2 = num2;
  }

  void cal() {
    if (choice == "+") {
      print('Result: ${num1 + num2}');
    } else if (choice == "-") {
      print('Result: ${num1 - num2}');
    } else if (choice == "*") {
      print('Result: ${num1 * num2}');
    } else if (choice == "/") {
      print('Result: ${num1 / num2}');
    }
  }
}

void main() {
  var play = Calculat();
  int one, two;
  String choice;

  print(' MENU \n Select the choice you want to perform : ');
  var menu = {
    "+": "ADD",
    '-': 'SUBTRACT',
    "*": "MULTIPT",
    '/': 'DIVIDE',
    "exit": "EXIT"
  };
  menu.forEach((n, s) => print(' ${n}. ${s}'));

  stdout.write('Choice you want to enter: ');
  choice = (stdin.readLineSync()!);

  if (choice == "exit") {
    print('Bye Bye. Thanks for using me.');
    exitCode;
  } else {
    stdout.write('Enter any one Number: ');
    one = int.parse(stdin.readLineSync()!);
    stdout.write('Enter any two Number: ');
    two = int.parse(stdin.readLineSync()!);

    play.getNum(one, choice, two);
    play.cal();
  }
}
